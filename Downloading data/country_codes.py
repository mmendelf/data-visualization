import json


def get_country_code(country_name):
    file = 'countries.json'
    with open(file) as f:
        countries = json.load(f)
        for code, name in countries.items():
            if name == country_name:
                return code.lower()
        return None

